<?php
get_header(); 
?>

<?php get_template_part('template-parts/hero'); ?>

<?php get_template_part('template-parts/featured-posts'); ?>

<?php get_template_part('template-parts/section'); ?>

<?php get_template_part('template-parts/featured-case-studies'); ?>

<?php get_template_part('template-parts/accreditations'); ?>

<?php get_template_part('template-parts/contact'); ?>

<?php
get_footer();