<?php
if (is_404()) :
	?>

	<form role="search" method="get" class="search-form flex flex-col " action="<?php echo home_url('/'); ?>">
		<label>
			<span class="screen-reader-text hidden"><?php echo _x('Search for:', 'label') ?></span>
			<div class="wrapper1 flex items-center border-b border-b-2 borderwhite py-2">
				<input type="text" class=" appearance-none bg-transparent border-none w-full text-white mr-3 py-1 px-2 leading-tight focus:outline-none" placeholder="<?php echo esc_attr_x('Search …', 'placeholder') ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
		</label>
		<input type="submit" class="search-submit w-64 p-2 rounded bg-blue" value="<?php echo esc_attr_x('Search', 'submit button') ?>" />
		</div>
	</form>

<?php

else :
	?>
	<form role="search" method="get" class="search-form flex flex-col " action="<?php echo home_url('/'); ?>">
		<label>
			<span class="screen-reader-text hidden"><?php echo _x('Search for:', 'label') ?></span>
			<div class="wrapper flex items-center flex-col py-2">
				<input type="text" class="mb-1" placeholder="<?php echo esc_attr_x('Search …', 'placeholder') ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label') ?>" />
		</label>
		<input type="submit" class="search-submit w-64 p-2 rounded" value="<?php echo esc_attr_x('Search', 'submit button') ?>" />
		</div>

	<?php

	endif;
