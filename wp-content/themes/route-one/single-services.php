<?php
get_header();
?>

<?php get_template_part('template-parts/intro-icon'); ?>

<?php get_template_part('template-parts/section'); ?>

<?php get_template_part('template-parts/latest-case-studies'); ?>

<?php get_template_part('template-parts/cta-banner'); ?>

<?php
get_footer();
