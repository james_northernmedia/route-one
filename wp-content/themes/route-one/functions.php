<?php
/**
 * Spike Starter Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package The_Starter_Theme
 */

if ( ! function_exists( 'spike_starter_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function spike_starter_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Spike Starter Theme, use a find and replace
		 * to change 'spike-starter' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'spike-starter', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary-menu'  => esc_html__( 'Primary', 'spike-starter' ),
            'footer-menu'   => esc_html__( 'Footer', 'spike-starter' ),
			'services-menu' => esc_html__( 'Services', 'spike-starter' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/**
		 * Add support for custom thumbnail sizes
        */

        /**
         * Add theme options page
        */

        if( function_exists('acf_add_options_page') ) {

           acf_add_options_page(array(
                'page_title'    => 'Options',
                'menu_title'    => 'Options',
                'menu_slug'     => 'theme-general-settings',
                'capability'    => 'edit_posts',
                'redirect'      => false,
                //'parent_slug' => 'options-general.php',
            ));
			
			acf_add_options_sub_page(array(
                'page_title'    => 'Sectors',
                'menu_title'    => 'Sectors',
                'menu_slug'     => 'sectors_settings',
				'parent_slug'	=> 'theme-general-settings',
            ));
			
			acf_add_options_sub_page(array(
                'page_title'    => 'Services',
                'menu_title'    => 'Services',
                'menu_slug'     => 'services_settings',
				'parent_slug'	=> 'theme-general-settings',
            ));
        }
	}
endif;
add_action( 'after_setup_theme', 'spike_starter_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function spike_starter_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'spike_starter_content_width', 640 );
}
add_action( 'after_setup_theme', 'spike_starter_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function spike_starter_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'spike-starter' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'spike-starter' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'spike_starter_widgets_init' );

/**
 * Prevent WP from loading jQuery by default.
 */
function remove_jquery_migrate( &$scripts){
    if(!is_admin()){
        $scripts->remove( 'jquery');
    }
}
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );

/**
 * Enqueue scripts and styles.
 */
function spike_starter_scripts() {
	wp_enqueue_style( 'spike-starter-style', get_stylesheet_uri() );
	
    wp_enqueue_script( 'theme.js', get_stylesheet_directory_uri() . '/public/js/theme.js', array(), '', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'spike_starter_scripts' );


/**
 * Add Admin & Login Styles
 */
function admin_custom_style() {
	wp_enqueue_style('admin-styles', get_stylesheet_directory_uri() . '/admin.css');
  }
  add_action('admin_enqueue_scripts', 'admin_custom_style');
  add_action('login_enqueue_scripts', 'admin_custom_style');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Remove unnecessary scripts to speed up WP
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Prevent WP from adding extra margin when logged-in
 */
function remove_admin_margin() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('admin_bar_init', 'remove_admin_margin');

/**
 * Ensure all mail is routed through Northern Media's SMTP relay, via Mailjet
 */
function amend_sender_email( $original_email_address ) {
    return 'website@northernmediauk.com';
}
add_filter( 'wp_mail_from', 'amend_sender_email' );

// disable Gutenberg for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// pagination
function paginate( $pages = '', $range = 2 ) {
	global $paged;
	$showitems = ( $range * 2 ) + 1;
	if ( empty( $paged ) ) { $paged = 1; }
	if ( $pages == '' ) {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if ( ! $pages ) { $pages = 1; }
	}
	if ( 1 != $pages ) {
		echo '<div align="center">';
		#echo '<div class="page-count">Page ' . $paged . ' of ' . $pages . '</div>';
		echo '<ul class="pagination">';
		if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) echo '<li class="first"><a href="' . get_pagenum_link( 1 ) . '"><i class="fas fa-angle-double-left"></i></a></li>';
		if ( $paged > 1 ) echo '<li class="prev"><a href="' . get_pagenum_link( $paged - 1 ) . '" rel="prev"><i class="fas fa-chevron-left"></i></a></li>';
		for ( $i=1; $i <= $pages; $i++ ) {
			if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged == $i ) ? '<li class="active"><span>'. $i .'</span></li>' : '<li><a href="' . get_pagenum_link( $i ) . '">' . $i . '</a></li>';
			}
		}
		if ( $paged < $pages ) echo '<li class="next"><a href="' . get_pagenum_link( $paged + 1 ) . '" rel="next"><i class="fas fa-chevron-right"></i></a></li>';
		if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) echo '<li class="last"><a href="' . get_pagenum_link( $pages ) . '"><i class="fas fa-angle-double-right"></i></a></li>';
		echo '</ul>';
		echo '</div>';
	}
}

//  Custom Post Types
function custom_post_type() {
	
	// Sectors - Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Sectors', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Sector', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Sectors', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Sector', 'twentytwenty' ),
        'all_items'           => __( 'All Sectors', 'twentytwenty' ),
        'view_item'           => __( 'View Sector', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Sector', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Sector', 'twentytwenty' ),
        'update_item'         => __( 'Update Sector', 'twentytwenty' ),
        'search_items'        => __( 'Search Sector', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
	// Sectors - Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'sectors', 'twentytwenty' ),
        'description'         => __( 'sectors', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 20,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' 		  => true,
 		'menu_icon'           => 'dashicons-chart-pie',
    );
    // Sectors - Registering your Custom Post Type
    register_post_type( 'sectors', $args );
	
	// Services - Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Services', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Service', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Services', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Service', 'twentytwenty' ),
        'all_items'           => __( 'All Services', 'twentytwenty' ),
        'view_item'           => __( 'View Service', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Service', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Service', 'twentytwenty' ),
        'update_item'         => __( 'Update Service', 'twentytwenty' ),
        'search_items'        => __( 'Search Service', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
	// Services - Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'services', 'twentytwenty' ),
        'description'         => __( 'Services', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attributes', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 20,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' 		  => true,
 		'menu_icon'           => 'dashicons-hammer',
    );
    // Services - Registering your Custom Post Type
    register_post_type( 'services', $args );
	
	//  Case Studies - Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Case Studies', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Case Study', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Case Studies', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Case Study', 'twentytwenty' ),
        'all_items'           => __( 'All Case Studies', 'twentytwenty' ),
        'view_item'           => __( 'View Case Study', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Case Study', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Case Study', 'twentytwenty' ),
        'update_item'         => __( 'Update Case Study', 'twentytwenty' ),
        'search_items'        => __( 'Search Case Study', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
	//  Case Studies - Set other options for Custom Post Type 
    $args = array(
        'label'               => __( 'case-studies', 'twentytwenty' ),
        'description'         => __( 'Case Studies', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 20,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' 		  => true,
 		'menu_icon'           => 'dashicons-portfolio',
		'taxonomies'          => array( 'category' ),
    );
    // Case Studies - Registering your Custom Post Type
    register_post_type( 'case-studies', $args );
	
	//  Resources - Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Resources', 'Post Type General Name', 'twentytwenty' ),
        'singular_name'       => _x( 'Resource', 'Post Type Singular Name', 'twentytwenty' ),
        'menu_name'           => __( 'Resources', 'twentytwenty' ),
        'parent_item_colon'   => __( 'Parent Resource', 'twentytwenty' ),
        'all_items'           => __( 'All Resources', 'twentytwenty' ),
        'view_item'           => __( 'View Resource', 'twentytwenty' ),
        'add_new_item'        => __( 'Add New Resource', 'twentytwenty' ),
        'add_new'             => __( 'Add New', 'twentytwenty' ),
        'edit_item'           => __( 'Edit Resource', 'twentytwenty' ),
        'update_item'         => __( 'Update Resource', 'twentytwenty' ),
        'search_items'        => __( 'Search Resource', 'twentytwenty' ),
        'not_found'           => __( 'Not Found', 'twentytwenty' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentytwenty' ),
    );
	//  Case Studies - Set other options for Custom Post Type 
    $args = array(
        'label'               => __( 'resources', 'twentytwenty' ),
        'description'         => __( 'Resources', 'twentytwenty' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 20,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' 		  => true,
 		'menu_icon'           => 'dashicons-book',
		'taxonomies'          => array( 'category' ),
    );
    // Case Studies - Registering your Custom Post Type
    register_post_type( 'resources', $args );
	
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );


// Allow SVG
add_filter('wp_check_filetype_and_ext', function ($data, $file, $filename, $mimes) {

    if (!$data['type']) {
        $wp_filetype = wp_check_filetype($filename, $mimes);
        $ext = $wp_filetype['ext'];
        $type = $wp_filetype['type'];
        $proper_filename = $filename;
        if ($type && 0 === strpos($type, 'image/') && $ext !== 'svg') {
            $ext = $type = false;
        }
        $data['ext'] = $ext;
        $data['type'] = $type;
        $data['proper_filename'] = $proper_filename;
    }
    return $data;


}, 10, 4);


add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});


add_action('admin_head', function () {
    echo '<style type="text/css">
         .media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }</style>';
});