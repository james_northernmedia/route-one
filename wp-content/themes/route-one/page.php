<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The_Starter_Theme
 */

get_header();
?>

<?php get_template_part('template-parts/intro'); ?>

<section class="section py-5">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php the_content(); ?>
			</div>
			
			<?php if ( has_post_thumbnail() ): ?>
			<div class="col-md-12 pt-5 text-center">
				<?php the_post_thumbnail('full', ['class' => 'img-fluid', 'title' => 'Featured image']); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php
get_footer();
