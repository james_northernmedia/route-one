<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Starter_Theme
 */

?>

</div>

<footer id="footer" class="section section--black py-5">
	<div class="container">
		
		<?php get_template_part('template-parts/footer-contact-form'); ?>
		
		<div class="row d-flex align-items-center">
			<div class="offset-4 offset-md-0 col-4 col-md-2">
				<?php
				$logo = get_field('secondary_logo', 'option');
				?>
				<a href="<?php echo site_url(); ?>/"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" id="logo" class="img-fluid"></a>
			</div>
			<div class="col-12 col-md-10 text-left text-md-right">
				<?php
				wp_nav_menu(array(
					'theme_location'	=> 'footer-menu'
				));
				?>
			</div>
		</div>
	</div>
</footer>

</div>

<?php wp_footer(); ?>

<script>
	/* Sample function that returns boolean in case the browser is Internet Explorer*/
	function isIE() {
		ua = navigator.userAgent;
		/* MSIE used to detect old browsers and Trident used to newer ones*/
		var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
		
		return is_ie; 
	}
	/* Create an alert to show if the browser is IE or not */
	if (isIE()){
		//alert('It is InternetExplorer');
		jQuery('.updateBrowser').addClass('updateBrowser--open');
	} else {
		//alert('It is NOT InternetExplorer');
		console.log('Good Browser');
	}
</script>

</body>
</html>