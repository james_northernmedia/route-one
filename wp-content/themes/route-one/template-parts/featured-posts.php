<?php if( have_rows('featured_posts') ):?>

<section class="featured-posts">
	<div class="container">
		
		<?php while ( have_rows('featured_posts') ) : the_row(); ?>
			<?php if( get_sub_field('show') == "intro"): ?>

				<?php if( have_rows('intro') ):
				while ( have_rows('intro') ) : the_row(); ?>

				<div class="row d-flex align-items-center py-5">
					<div class="col-md-12">
						<?php the_sub_field('text'); ?>

						<?php if( have_rows('button') ):
						while ( have_rows('button') ) : the_row(); ?>
							<?php if( get_sub_field('text') ): ?>

							<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

							<?php endif; ?>

						<?php endwhile; else : ?>
						<?php endif; ?>
					</div>
				</div>

				<?php endwhile; ?>
				<?php endif; ?>
		
			<?php endif; ?>
		<?php endwhile; ?>
		
		<div class="row pt-5 pt-lg-0">
			
			<?php
			
			while ( have_rows('featured_posts') ) : the_row();
				$posttype = get_sub_field('post_type');
			endwhile;
			
			$args = array(
				'post_type'      => $posttype,
				'posts_per_page' => 5,
				'order' => 'ASC'
			);
			
			$loop = new WP_Query( $args );

			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			<a class="col-lg featured-post text-center square my-3 my-lg-0" href="<?php the_permalink(); ?>">
				<div class="icon mb-5 d-inline-block">
					<object data="<?php the_field('image'); ?>" type="image/svg+xml"></object>
				</div>
				<h4><?php the_title(); ?></h4>
			</a>
			
			<?php endwhile;
			wp_reset_postdata();
			?>

		</div>
		
		<?php while ( have_rows('featured_posts') ) : the_row(); ?>
			<?php if( get_sub_field('show') == "cta"): ?>

				<?php if( have_rows('cta') ):
				while ( have_rows('cta') ) : the_row(); ?>

				<div class="row d-flex align-items-center square my-5">
					<div class="col-md-8">
						<?php the_sub_field('text'); ?>
					</div>
					<div class="col-md-4 text-right">
						<?php if( have_rows('button') ):
						while ( have_rows('button') ) : the_row(); ?>
							<?php if( get_sub_field('text') ): ?>

							<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

							<?php endif; ?>

						<?php endwhile; else : ?>
						<?php endif; ?>
					</div>
				</div>

				<?php endwhile; ?>
				<?php endif; ?>

			<?php endif; ?>
		<?php endwhile; ?>
		
	</div>
</section>

<?php endif; ?>