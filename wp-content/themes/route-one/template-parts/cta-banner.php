<?php if( have_rows('cta_banner') ):
while ( have_rows('cta_banner') ) : the_row(); ?>

<section class="section">
	<div class="container">
		<div class="row section--l-blue p-5">
			<div class="col-md-12 text-center">
				<?php the_sub_field('text'); ?>
			</div>
			<div class="col-md-12 text-center d-<?php the_sub_field('show_contact_info'); ?>">
				<p>
					<a href="tel:<?php the_field('phone', 'option'); ?>" class="red mr-3"><?php the_field('phone', 'option'); ?></a>
					<a href="mailto:<?php the_field('email', 'option'); ?>" class="red"><?php the_field('email', 'option'); ?></a>
				</p>
			</div>
			<div class="col-md-12">
				<?php if( have_rows('button') ): ?>

				<div class="button-container text-center">

					<?php while ( have_rows('button') ) : the_row(); ?>
						<?php if( get_sub_field('text') ): ?>


							<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

						<?php endif; ?>
					<?php endwhile; ?>

				</div>

				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>
<?php endif; ?>