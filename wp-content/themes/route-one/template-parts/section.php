<?php if( have_rows('section') ):
while ( have_rows('section') ) : the_row(); ?>

<section class="section section--<?php the_sub_field('position'); ?> py-5">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<img src="<?php the_sub_field('image'); ?>" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php the_sub_field('text'); ?>

					<?php if( have_rows('button') ): ?>
					
					<div class="button-container">
						
						<?php while ( have_rows('button') ) : the_row(); ?>
							<?php if( get_sub_field('text') ): ?>


								<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

							<?php endif; ?>
						<?php endwhile; ?>
						
					</div>
					
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>
<?php endif; ?>