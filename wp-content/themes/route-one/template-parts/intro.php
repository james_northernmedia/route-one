<?php if( have_rows('intro') ):
while ( have_rows('intro') ) : the_row(); ?>

<section class="section section--intro section--d-blue">
	<div class="container">
		<div class="row">
			
			<?php if( get_sub_field('title') ): ?>
			<div class="col-md-6">
				<?php the_sub_field('title'); ?>
			</div>
			
			<div class="col-md-6">
				<div class="text-md-right">
					<?php if( have_rows('button') ):
					while ( have_rows('button') ) : the_row(); ?>

						<?php if( get_sub_field('text') ): ?>
						
						<div class="button-container">
							<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>
						</div>

						<?php endif; ?>

					<?php endwhile; else : ?>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			
			<?php if( get_sub_field('text') ): ?>
			<div class="col-md-12 <?php if( get_sub_field('title') ): ?>pt-5<?php endif; ?>">
				<?php the_sub_field('text'); ?>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</section>

<?php get_template_part('template-parts/breadcrumbs'); ?>

<?php endwhile; ?>
<?php endif; ?>