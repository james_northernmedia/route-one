<?php if( have_rows('latest_case_studies') ):?>

<section class="latest-case-studies">
	<div class="container">
		
		<?php while ( have_rows('latest_case_studies') ) : the_row(); ?>
		
			<?php if( have_rows('intro') ):
			while ( have_rows('intro') ) : the_row(); ?>

			<div class="row d-flex align-items-center py-5">
				<div class="col-md-12">
					<?php the_sub_field('text'); ?>

					<?php if( have_rows('button') ):
					while ( have_rows('button') ) : the_row(); ?>
						<?php if( get_sub_field('text') ): ?>

						<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

						<?php endif; ?>

					<?php endwhile; else : ?>
					<?php endif; ?>
				</div>
			</div>

			<?php endwhile; ?>
			<?php endif; ?>
		
		<?php endwhile; ?>
		
		<div class="row pt-5 pt-lg-0">
			
			<?php
			
			$args = array(
				'post_type'      => "case-studies",
				'posts_per_page' => 3,
				'order' => 'ASC'
			);
			
			$loop = new WP_Query( $args );

			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			<a class="col-lg latest-case-study my-3 my-lg-0" href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('medium_large', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
				<h3 class="pt-5"><?php the_date('d/m/Y'); ?></h3>
				<h4><?php the_title(); ?></h4>
			</a>
			
			<?php endwhile;
			wp_reset_postdata();
			?>

		</div>
		
	</div>
</section>

<?php endif; ?>