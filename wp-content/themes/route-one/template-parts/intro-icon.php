<?php if( get_field('image') ): ?>

<section class="section section--intro section--d-blue">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="icon">
					<object data="<?php the_field('image'); ?>" type="image/svg+xml"></object>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('template-parts/breadcrumbs'); ?>

<?php endif; ?>