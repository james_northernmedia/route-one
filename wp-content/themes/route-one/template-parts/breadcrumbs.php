<section class="section py-5">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<p><a href="javascript:history.back()">Go Back</a></p>
			</div>
			<div class="col-6 text-right">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
			</div>
		</div>
	</div>
</section>