<?php if( have_rows('testimonial', 'option') ): ?>

<?php the_field('testimonials_title', 'option'); ?>

<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2020/12/speech-marks-e1608741075915.png" alt="Speech Marks" class="mb-5">

<!-- Slider -->
<div class="swiper-container">
	<div class="swiper-wrapper">

		<?php while ( have_rows('testimonial', 'option') ) : the_row(); ?>

		<!-- Slide -->
		<div class="swiper-slide">
			<div class="slide">
				<?php the_sub_field('text', 'option'); ?>
				<p>
					<strong><?php the_sub_field('name', 'option'); ?></strong><br/>
					<span class="green"><?php the_sub_field('title', 'option'); ?>, <?php the_sub_field('company', 'option'); ?></span>
				</p>
			</div>
		</div>

		<?php endwhile; ?>

	</div>
	<div class="swiper-pagination mt-5"></div>
</div>

<?php endif; ?>