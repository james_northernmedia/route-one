<?php if( have_rows('partner', 'option') ): ?>

<?php the_field('partners_title', 'option'); ?>

<div class="row partners pt-5">
	
	<?php while ( have_rows('partner', 'option') ) : the_row(); ?>
	
	<div class="col-4">
		<img src="<?php the_sub_field('logo', 'option'); ?>" alt="Partner" class="img-fluid partner">
	</div>
	
	<?php endwhile; ?>
	
</div>

<?php endif; ?>