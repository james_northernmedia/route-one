<section class="section section--lined my-5">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php get_template_part('template-parts/partners'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php get_template_part('template-parts/testimonials'); ?>
				</div>
			</div>
		</div>
	</div>
</section>