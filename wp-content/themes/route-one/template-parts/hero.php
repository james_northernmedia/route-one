<div class="hero">
	<section class="slider">
		
		<!-- Video -->
		<?php if( have_rows('video') ):
		while ( have_rows('video') ) : the_row(); ?>

		<section class="video">
			<div class="overlay"></div>
			<video loop="true" autoplay="autoplay" muted="false" playsinline="">
				<source src="<?php the_sub_field('mp4'); ?>" type="video/mp4">
				<source src="<?php the_sub_field('ogg'); ?>" type="video/ogg">
				<source src="<?php the_sub_field('webm'); ?>" type="video/webm">
			</video>
		</section>

		<?php endwhile; ?>
		<?php endif; ?>
		
		<!-- Slider -->
		<?php if( have_rows('slide') ): ?>
		
		<div class="swiper-container">
			<div class="swiper-wrapper">
				
				<?php while ( have_rows('slide') ) : the_row(); ?>
				
				<!-- Slide -->
				<div class="swiper-slide">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="slide">
									<?php the_sub_field('text'); ?>
									
									<?php if( have_rows('button') ):
									while ( have_rows('button') ) : the_row(); ?>

										<?php if( get_sub_field('text') ): ?>

										<a href="<?php the_sub_field('link'); ?>" class="mt-5 button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

										<?php endif; ?>

									<?php endwhile; else : ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<?php endwhile; ?>
			
			</div>
		</div>
		
		<?php endif; ?>
		
	</section>
</div>




