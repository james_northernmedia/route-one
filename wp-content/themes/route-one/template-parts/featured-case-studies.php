<section class="section py-5">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<img src="http://localhost/route-one/wp-content/uploads/2020/12/map.png" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<h3>Case Studies</h3>
					<h2>Areas we cover</h2>
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
</section>