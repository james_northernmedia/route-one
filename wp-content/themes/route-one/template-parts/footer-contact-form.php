<?php if( get_field('show_form') == "yes"): ?>

<?php if( have_rows('footer_contact_form', 'option') ): ?>

<div class="row d-flex align-items-center pb-5 mb-5">
	<div class="col-md-6">
		<div class="py-3 p-md-0">
			<?php while ( have_rows('footer_contact_form', 'option') ) : the_row(); ?>
				<?php the_sub_field('text'); ?>
			<?php endwhile; ?>

			<div class="row d-flex align-items-center">
				<div class="col-2">
					<i class="fas fa-comment footer-icon"></i>
				</div>
				<div class="col-10 pl-0">
					<p>
						<a href="tel:<?php the_field('phone', 'option'); ?>" class="red"><?php the_field('phone', 'option'); ?></a><br/>
						<a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
					</p>
				</div>
				<div class="col-2">
					<i class="fas fa-map-marker footer-icon"></i>
				</div>
				<div class="col-10 pl-0">
					<?php the_field('address', 'option'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="py-3 p-md-0">
			<?php while ( have_rows('footer_contact_form', 'option') ) : the_row(); ?>
				<?php the_sub_field('form'); ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>

<?php endif; ?>

<?php endif; ?>