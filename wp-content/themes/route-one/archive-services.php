<?php
get_header();
?>

<?php get_template_part('template-parts-archive/archive-intro'); ?>

<?php get_template_part('template-parts-archive/archive-featured-posts'); ?>

<?php get_template_part('template-parts-archive/services-section'); ?>

<?php
get_footer();
?>