const path = require("path");
const common = require("./webpack.common");
const merge = require("webpack-merge");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");

module.exports = merge(common, {
	mode: "development",
	output: {
		filename: "theme.js",
		path: path.resolve(__dirname, "public/js/")
	},
	plugins: [
		new BrowserSyncPlugin(
			// BrowserSync options
			{
				port: 3000,
				files: ["**/*"],
				proxy: "http://localhost/route-one/"
			},
			// plugin options
			{
				reload: false,
				injectCss: true
			}
		)
	],
	module: {
		rules: [
			{
				test: /\.(gif|svg|jpg|png)$/,
				  loader: "file-loader",
				  options: {
					name: '[path][name].[ext]',
					esModule: false,
				  },
			}, {
				test: /\.(gif|svg|jpg|png)$/,
				  loader: "file-loader",
				  options: {
					name: '[path][name].[ext]',
					publicPath: (url, resourcePath, context) => {
						// `resourcePath` is original absolute path to asset
						// `context` is directory where stored asset (`rootContext`) or `context` option
			
						// To get relative path you can use
						// const relativePath = path.relative(context, resourcePath);
			
						if (/my-custom-image\.png/.test(resourcePath)) {
						  return `other_public_path/${url}`;
						}
			
						if (/images/.test(context)) {
						  return `image_output_path/${url}`;
						}
			
						return `wp-content/themes/The%20Digital%20Alliance//${url}`;
					  },
					esModule: false,
				  },
			}, {
				test: /\.scss$/,
				use: [
					{ loader: "style-loader" }, //Imports changed files into the DOM
					{
						loader: "css-loader",
						options: {
							sourceMap: true
						}
					}, // Used for the @import in the js file
					{
						loader: "postcss-loader", //Multiple css options to optimise the code
						options: {
							plugins: [
								require("precss"), //Lets you use Sass-like markup and staged CSS features in CSS
								require("autoprefixer"), //Parse CSS and add vendor prefixes to CSS
								require("postcss-import")() //Allows @ import
							]
						}
					},
					{ loader: "sass-loader" } //Compiles SASS to CSS
				]
			}
		]
	}
});