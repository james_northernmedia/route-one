//************* */
// Imports
//************* */

// Font Awesome
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

// jQuery - https://jquery.com/
import $ from "jquery";

// Swiper Slider - https://idangero.us/swiper/ (JS Image Slider)
import Swiper from "swiper";

// QuickLink Import used to prefetch internal links
import { listen, prefetch } from "quicklink";

// Webpack SCSS import for Developmend Mode
import "../sass/main.scss";

//************************** */
// Page ready to fire code
//************************** */

$(document).ready(function() {

	// quicklink prefetch
	listen();

	// Enable Hover touch on mobile
	$('body').on('touchstart', function() {});
	
	// Swiper Slider
	var swiper = new Swiper(".swiper-container", {
		centeredSlides: true,
		autoplay: {
			delay: 10000,
			disableOnInteraction: false
		},
		pagination: {
			el: ".swiper-pagination",
			clickable: true
		}
	});

	// Fire Google event when 'tel:' or 'mailto:' links are clicked
	$("a[href^='tel:']").click(function() {
		gtag("event", "Contact via phone", {
			event_category: "Contact via phone ",
			event_label: "click",
			value: 1
		});
	});

	$("a[href^='mailto:']").click(function() {
		gtag("event", "Contact via email", {
			event_category: "Contact via email",
			event_label: "click",
			value: 2
		});
	});

	// Close Browser Update
	$('.updateBrowser .close').click( function() {
		$('.updateBrowser .close').removeClass('updateBrowser--open');
	});
	
	// Menu Toggle
	$('#menu-toggle').click( function() {
		var toggleWidth = $("#menu-primary-navigation").width() == 0 ? "100%" : "0";
		$('#menu-primary-navigation').animate({ width: toggleWidth });
	});
	
	// Add smooth scrolling to all links
	$("a").on('click', function(event) {

		if (this.hash !== "") {
		  event.preventDefault();
			
		  var hash = this.hash;
			
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 800, function(){
			window.location.hash = hash;
		  });
		}
	});
	
	// Slick Slider
	$('.hero-slider').slick({
		infinite: true
	});

});

// Sticky Navigation
$(window).scroll(function(){
	if ($(this).scrollTop() > 1) {
		$('#header').addClass('scrolled');
	} else {
		$('#header').removeClass('scrolled');
	}
});

// Footer Accordion
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    /* Toggle between adding and removing the "active" class,
    to highlight the button that controls the panel */
    this.classList.toggle("active");

    /* Toggle between hiding and showing the active panel */
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}