<!--

A website by Northern Media.

Get in touch - hello@northernmediauk.com

-->
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site antialiased">

		<section id="header">
			<nav>
				<div class="container">
					<div class="row d-flex align-items-center py-4 py-lg-0">
						<div class="col-4 col-md-2">
							<?php
							$logo = get_field('main_logo', 'option');
							?>
							<a href="<?php echo site_url(); ?>/"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" id="logo" class="img-fluid"></a>
						</div>
						<div class="col-8 col-md-10 text-right">
							<div id="menu-toggle" class="d-inline-block d-lg-none text-right">
								<i class="fas fa-bars"></i>
							</div>
							<?php
							wp_nav_menu(array(
								'theme_location'	=> 'primary-menu'
							));
							?>
						</div>
					</div>
				</div>
			</nav>
		</section>

		<?php /* get_template_part( 'template-parts/partial', 'browser' ); */ ?>

		<div id="content" class="site-content">