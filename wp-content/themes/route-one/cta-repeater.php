<?php
/*
*
* Template Name: CTA Repeater Template
* 
*/
get_header();
?>

<section id="form" class="section section--intro py-5">
	<div class="container">
		<div class="row d-flex align-items-center pb-5 mb-5">
			<div class="col-md-6">
				<div class="py-3 p-md-0">
					<h1 class="pb-4"><?php the_title(); ?></h1>
					<div class="pb-4">
						<?php the_content(); ?>
					</div>
					
					<?php if( get_field('form') ): ?>
						<div class="row d-flex align-items-center">
							<div class="col-2">
								<i class="fas fa-comment footer-icon"></i>
							</div>
							<div class="col-10 pl-0">
								<p>
									<a href="tel:<?php the_field('phone', 'option'); ?>" class="red"><?php the_field('phone', 'option'); ?></a><br/>
									<a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
								</p>
							</div>
							<div class="col-2">
								<i class="fas fa-map-marker footer-icon"></i>
							</div>
							<div class="col-10 pl-0">
								<?php the_field('address', 'option'); ?>
							</div>
						</div>
					<?php endif; ?>
					
				</div>
			</div>
			<?php if( get_field('form') ): ?>
			<div class="col-md-6">
				<div class="py-3 p-md-0">
					<?php the_field('form'); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php if( have_rows('cta') ):
while ( have_rows('cta') ) : the_row(); ?>

<section class="section py-5">
	<div class="container">
		<div class="row section--l-blue">
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php the_sub_field('title'); ?>
					<?php if( have_rows('button') ): ?>
					
					<div class="button-container">
						
						<?php while ( have_rows('button') ) : the_row(); ?>
							<?php if( get_sub_field('text') ): ?>


								<a href="<?php the_sub_field('link'); ?>" class="button button--<?php the_sub_field('style'); ?>"><?php the_sub_field('text'); ?></a>

							<?php endif; ?>
						<?php endwhile; ?>
						
					</div>
					
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php the_sub_field('text'); ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>
<?php endif; ?>


<?php
get_footer();
?>