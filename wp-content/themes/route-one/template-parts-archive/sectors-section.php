<?php if( have_rows('sectors_section', 'options') ):?>

<section class="section">
	<div class="container">
		<div class="row">
			
			<?php while ( have_rows('sectors_section', 'options') ) : the_row(); ?>
			
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php the_sub_field('text', 'options'); ?>
					<img src="<?php the_sub_field('image', 'options'); ?>" alt="" class="img-fluid mt-5">

					<?php if( have_rows('button', 'options') ): ?>
						<div class="button-container">

							<?php while ( have_rows('button', 'options') ) : the_row(); ?>
								<?php if( get_sub_field('text', 'options') ): ?>

									<a href="<?php the_sub_field('link', 'options'); ?>" class="button button--<?php the_sub_field('style', 'options'); ?>"><?php the_sub_field('text', 'options'); ?></a>

								<?php endif; ?>
							<?php endwhile; ?>

						</div>
					<?php endif; ?>
				</div>
			</div>
			
			<?php endwhile; ?>
			
		</div>
	</div>
</section>

<?php endif; ?>