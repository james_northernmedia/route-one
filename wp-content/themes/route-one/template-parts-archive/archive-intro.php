<?php
if( get_post_type() == 'sectors' ) {
	$posttype_acf = 'sectors_intro';
} elseif( get_post_type() == 'services' ) {
	$posttype_acf = 'services_intro';
} else {
	
}
?>

<?php if( have_rows($posttype_acf, 'options') ):
while ( have_rows($posttype_acf, 'options') ) : the_row(); ?>

<section class="section section--intro section--d-blue">
	<div class="container">
		<div class="row">
			
			<?php if( get_sub_field('title', 'options') ): ?>
			<div class="col-md-6">
				<?php the_sub_field('title', 'options'); ?>
			</div>
			
			<div class="col-md-6">
				<div class="text-md-right">
					<?php if( have_rows('button', 'options') ):
					while ( have_rows('button', 'options') ) : the_row(); ?>

						<?php if( get_sub_field('text', 'options') ): ?>
						
						<div class="button-container">
							<a href="<?php the_sub_field('link', 'options'); ?>" class="button button--<?php the_sub_field('style', 'options'); ?>"><?php the_sub_field('text', 'options'); ?></a>
						</div>

						<?php endif; ?>

					<?php endwhile; else : ?>
					<?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			
			<?php if( get_sub_field('text', 'options') ): ?>
			<div class="col-md-12 <?php if( get_sub_field('title', 'options') ): ?>pt-5<?php endif; ?>">
				<?php the_sub_field('text', 'options'); ?>
			</div>
			<?php endif; ?>
			
		</div>
	</div>
</section>

<section class="section pt-5">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<p><a href="javascript:history.back()">Go Back</a></p>
			</div>
			<div class="col-6 text-right">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>
<?php endif; ?>