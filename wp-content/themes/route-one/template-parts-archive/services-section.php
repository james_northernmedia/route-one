<?php if( have_rows('services_section', 'options') ):
while ( have_rows('services_section', 'options') ) : the_row(); ?>

<section class="section section--<?php the_sub_field('position', 'options'); ?> py-5">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<img src="<?php the_sub_field('image', 'options'); ?>" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6">
				<div class="py-3 p-md-5">
					<?php the_sub_field('text', 'options'); ?>

					<?php if( have_rows('button', 'options') ): ?>
					
					<div class="button-container">
						
						<?php while ( have_rows('button', 'options') ) : the_row(); ?>
							<?php if( get_sub_field('text', 'options') ): ?>


								<a href="<?php the_sub_field('link', 'options'); ?>" class="button button--<?php the_sub_field('style', 'options'); ?>"><?php the_sub_field('text', 'options'); ?></a>

							<?php endif; ?>
						<?php endwhile; ?>
						
					</div>
					
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>
<?php endif; ?>