<?php
if( get_post_type() == 'sectors' ) {
	$posttype_acf = 'sectors_featured_posts';
} elseif( get_post_type() == 'services' ) {
	$posttype_acf = 'services_featured_posts';
} else {
	
}

if( get_post_type() == 'sectors' ) {
	$posttype = 'sectors';
} elseif( get_post_type() == 'services' ) {
	$posttype = 'services';
} else {
	
}
?>

<?php if( have_rows($posttype_acf, 'options') ):?>

<section class="featured-posts">
	<div class="container">
		
		<?php while ( have_rows($posttype_acf, 'options') ) : the_row(); ?>
			<?php if( get_sub_field('show', 'options') == "intro"): ?>

				<?php if( have_rows('intro', 'options') ):
				while ( have_rows('intro', 'options') ) : the_row(); ?>

				<div class="row d-flex align-items-center py-5">
					<div class="col-md-12">
						<?php the_sub_field('text', 'options'); ?>

						<?php if( have_rows('button', 'options') ):
						while ( have_rows('button', 'options') ) : the_row(); ?>
							<?php if( get_sub_field('text', 'options') ): ?>

							<a href="<?php the_sub_field('link', 'options'); ?>" class="button button--<?php the_sub_field('style', 'options'); ?>"><?php the_sub_field('text', 'options'); ?></a>

							<?php endif; ?>

						<?php endwhile; else : ?>
						<?php endif; ?>
					</div>
				</div>

				<?php endwhile; ?>
				<?php endif; ?>
		
			<?php endif; ?>
		<?php endwhile; ?>
		
		<div class="row pt-5 pt-lg-0">
			
			<?php
			
			$args = array(
				'post_type'      => $posttype,
				'posts_per_page' => 5,
				'order' => 'ASC'
			);
			
			$loop = new WP_Query( $args );

			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			
			<a class="col-lg featured-post text-center square my-3 my-lg-0" href="<?php the_permalink(); ?>">
				<div class="icon mb-5 d-inline-block">
					<object data="<?php the_field('image'); ?>" type="image/svg+xml"></object>
				</div>
				<h4><?php the_title(); ?></h4>
			</a>
			
			<?php endwhile;
			wp_reset_postdata();
			?>

		</div>
		
		<?php while ( have_rows($posttype_acf, 'options') ) : the_row(); ?>
			<?php if( get_sub_field('show', 'options') == "cta"): ?>

				<?php if( have_rows('cta', 'options') ):
				while ( have_rows('cta', 'options') ) : the_row(); ?>

				<div class="row d-flex align-items-center square my-5">
					<div class="col-md-8">
						<?php the_sub_field('text', 'options'); ?>
					</div>
					<div class="col-md-4 text-right">
						<?php if( have_rows('button', 'options') ):
						while ( have_rows('button', 'options') ) : the_row(); ?>
							<?php if( get_sub_field('text', 'options') ): ?>

							<a href="<?php the_sub_field('link', 'options'); ?>" class="button button--<?php the_sub_field('style', 'options'); ?>"><?php the_sub_field('text', 'options'); ?></a>

							<?php endif; ?>

						<?php endwhile; else : ?>
						<?php endif; ?>
					</div>
				</div>

				<?php endwhile; ?>
				<?php endif; ?>

			<?php endif; ?>
		<?php endwhile; ?>
		
	</div>
</section>

<?php endif; ?>