<?php
get_header();
?>

<section class="section section--white">
	<div class="container">
		<div class="row posts">
			
			<div class="col-12 pb-5">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
					  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
			</div>
			
			<div class="col-12">
				<p>Sorry, no posts were found.</p>
				<a href="/" class="button button--green">Go Home</a>
			</div>
			
		</div>
	</div>
</section>

<?php
get_footer();
?>