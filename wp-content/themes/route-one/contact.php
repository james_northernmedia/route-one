<?php
/*
*
* Template Name: Contact
* 
*/
get_header();
?>

<section class="section section--intro py-5">
	<div class="container">
		<div class="row d-flex align-items-center pb-5 mb-5">
			<div class="col-md-6">
				<div class="py-3 p-md-0">
					<h1 class="pb-4"><?php the_title(); ?></h1>
					<div class="pb-4">
						<?php the_content(); ?>
					</div>
					
					<?php if( get_field('form') ): ?>
						<div class="row d-flex align-items-center">
							<div class="col-2">
								<i class="fas fa-comment footer-icon"></i>
							</div>
							<div class="col-10 pl-0">
								<p>
									<a href="tel:<?php the_field('phone', 'option'); ?>" class="red"><?php the_field('phone', 'option'); ?></a><br/>
									<a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
								</p>
							</div>
							<div class="col-2">
								<i class="fas fa-map-marker footer-icon"></i>
							</div>
							<div class="col-10 pl-0">
								<?php the_field('address', 'option'); ?>
							</div>
						</div>
					<?php endif; ?>
					
				</div>
			</div>
			<?php if( get_field('form') ): ?>
			<div class="col-md-6">
				<div class="py-3 p-md-0">
					<?php the_field('form'); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>

<iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=1%20Innovation%20Square,%20Featherstone,%20Pontefract%20WF7%206NX+(Route%20One%20Infrastructure)&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>

<?php
get_footer();
?>