<?php
/*
*
* Template Name: About
* 
*/
get_header();
?>

<?php get_template_part('template-parts/intro'); ?>

<?php get_template_part('template-parts/section'); ?>

<?php get_template_part('template-parts/featured-posts'); ?>

<?php get_template_part('template-parts/accreditations'); ?>

<?php
get_footer();
?>